const init = require('./index')

init.listen(5000, () => {
    console.log('Servidor rodando na porta 5000');
    console.log('http://localhost:5000/hotel/');
})