const router = require('express').Router()
const ReservasController = require('../controller/reservasController');
const usuarioController = require('../controller/usuarioController'); 
const QuartosController = require('../controller/quartosController');


router.post('/quartosPost/', QuartosController.criarQuarto);
router.get('/quartosGet/', QuartosController.buscarTodosQuartos);
router.get('/quartosGet/:id', QuartosController.buscarQuartoPorId);
router.put('/quartosAtualiza/:id', QuartosController.atualizarQuarto);
router.delete('/quartosDeleta/:id', QuartosController.excluirQuarto);

router.post('/usuariosPost/', usuarioController.criarUsuario ); 
router.get('/usuariosGet/', usuarioController.buscarTodosUsuarios);
router.get('/usuariosGet/:id', usuarioController.buscarUsuarioPorId);
router.put('/usuariosAtualiza/:id', usuarioController.atualizarUsuario);
router.delete('/usuariosDeleta/:id', usuarioController.excluirUsuario);
router.post('/login', usuarioController.login);

router.post('/reservasPost/', ReservasController.criarReserva);
router.get('/reservasGet/', ReservasController.buscarTodasReservas);
router.get('/reservasDetails/', ReservasController.detalhesReservas);
router.get('/reservasGet/:id', ReservasController.buscarReservaPorId);
router.put('/reservasAtualiza/:id', ReservasController.atualizarReserva);
router.delete('/reservasDeleta/:id', ReservasController.excluirReserva);

module.exports = router; 
