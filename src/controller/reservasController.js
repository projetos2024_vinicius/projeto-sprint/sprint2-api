const db = require('../db/connect');  // Importa a conexão com o banco de dados

class ReservaController {
  // Método para criar uma nova reserva
  static async criarReserva(req, res) {
    const { id_quarto, id_usuarios, data_entrada, data_saida } = req.body;
    console.log("Dados recebidos:", req.body);

    // Verifica se todos os campos obrigatórios foram preenchidos
    if (!id_quarto || !id_usuarios || !data_entrada || !data_saida) {
      return res.status(400).json({ error: 'Preencha os campos corretamente.' });
    }

    // Verifica se a data de entrada é maior que a data de saída
    if (data_entrada > data_saida) {
      return res.status(400).json({ error: 'Data de entrada não pode ser maior que a data de saída.' });
    }

    // Verifica se a data de entrada e saída são iguais
    if (data_entrada === data_saida) {
      return res.status(400).json({ error: 'Data de entrada e saída não podem ser iguais.' });
    }

    const currentDate = new Date().toISOString().split('T')[0]; // Data atual no formato YYYY-MM-DD
    // Verifica se a data de entrada é anterior à data atual
    if (data_entrada < currentDate) {
      return res.status(400).json({ error: 'Data de entrada não pode ser anterior à data atual.' });
    }

    try {
      // Verifica se já existe uma reserva para o quarto no período solicitado

      /* A query seleciona todas as reservas que tenha um id de quarto específico
       e que verifica se a data de entrada da reserva é anterior à data fornecida */

      const [overlapResults] = await db.execute(`
        SELECT * FROM reservas
        WHERE id_quarto = ?
        AND (data_entrada < ? AND data_saida > ?)`, [id_quarto, data_saida, data_entrada]);

      if (overlapResults.length > 0) {
        console.log("Quarto já reservado para o período solicitado");
        return res.status(400).json({ error: "Quarto já reservado para o período solicitado" });
      }

      // Obtém o valor da diária do quarto
      // Faz um select no valor_diaria dos quartos que tem um id específico
      const [quartoResults] = await db.execute('SELECT valor_diaria FROM quartos WHERE id = ?', [id_quarto]);
      if (quartoResults.length === 0) {
        return res.status(404).json({ error: 'Quarto não encontrado.' });
      }
      const valorDiaria = quartoResults[0].valor_diaria;

      // Calcula a diferença de dias entre as datas de entrada e saída
      const diffTime = new Date(data_saida) - new Date(data_entrada);
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); // Diferença em dias
      const valorTotal = valorDiaria * diffDays;

      // Insere a nova reserva no banco de dados com seus respectivos dados:
      await db.execute(`
        INSERT INTO reservas (id_quarto, id_usuarios, data_entrada, data_saida, valor_total)
        VALUES (?, ?, ?, ?, ?)`, [id_quarto, id_usuarios, data_entrada, data_saida, valorTotal]);

      console.log("Reserva cadastrada com sucesso");
      return res.status(201).json({ message: "Reserva cadastrada com sucesso" });

    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno de servidor" });
    }
  }

  // Método para buscar todas as reservas
  static async buscarTodasReservas(req, res) {
    try {

      // Seleciona todas as reservas pelo select
      const [results] = await db.execute('SELECT * FROM reservas');
      return res.status(200).json(results);
    } catch (error) {
      return res.status(500).json({ error: 'Erro ao buscar reservas: ' + error.message });
    }
  }

  // Método para buscar uma reserva pelo ID
  static async buscarReservaPorId(req, res) {
    const id = req.params.id;

    try {
      // Seleciona uma reserva pelo id 
      const [results] = await db.execute('SELECT * FROM reservas WHERE id = ?', [id]);

      if (results.length === 0) {
        return res.status(404).json({ error: 'Reserva não encontrada.' });
      }

      return res.status(200).json(results[0]);
    } catch (error) {
      return res.status(500).json({ error: 'Erro ao buscar reserva: ' + error.message });
    }
  }

  // Método para atualizar uma reserva existente
  static async atualizarReserva(req, res) {
    const id = req.params.id;
    const { id_quarto, id_usuarios, data_entrada, data_saida } = req.body;
  
    // Verificações de validação dos dados
    if (!id_quarto || !id_usuarios || !data_entrada || !data_saida) {
      return res.status(400).json({ error: 'Preencha os campos corretamente.' });
    }
  
    if (data_entrada > data_saida) {
      return res.status(400).json({ error: 'Data de entrada não pode ser maior que a data de saída.' });
    }
  
    if (data_entrada === data_saida) {
      return res.status(400).json({ error: 'Data de entrada e saída não podem ser iguais.' });
    }
  
    const currentDate = new Date().toISOString().split('T')[0]; // Data atual no formato YYYY-MM-DD
    if (data_entrada < currentDate) {
      return res.status(400).json({ error: 'Data de entrada não pode ser anterior à data atual.' });
    }
  
    try {
      // Verifica se a reserva existe pelo id 
      const [reservaResults] = await db.execute('SELECT * FROM reservas WHERE id = ?', [id]);
      if (reservaResults.length === 0) {
        return res.status(404).json({ error: 'Reserva não encontrada.' });
      }
  
      // Verifica se há conflitos de datas com outras reservas
      // Seleciona todas as reservas com id de quarto específico, id de reserva e data de entrada e saída 
      const [overlapResults] = await db.execute(
        'SELECT * FROM reservas WHERE id_quarto = ? AND id != ? AND (data_entrada < ? AND data_saida > ?)',
        [id_quarto, id, data_saida, data_entrada]
      );
      if (overlapResults.length > 0) {
        return res.status(400).json({ error: 'As datas selecionadas conflitam com outra reserva.' });
      }
  
      // Obtém o valor da diária do quarto pelo id
      // Seleciona o valor da diária dos quartos que tem um id específico 
      const [quartoResults] = await db.execute('SELECT valor_diaria FROM quartos WHERE id = ?', [id_quarto]);
      if (quartoResults.length === 0) {
        return res.status(404).json({ error: 'Quarto não encontrado.' });
      }
      const valorDiaria = quartoResults[0].valor_diaria;
  
      // Calcula a diferença de dias entre as datas de entrada e saída
      const diffTime = new Date(data_saida) - new Date(data_entrada);
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); // Diferença em dias
      const valorTotal = valorDiaria * diffDays;
  
      // Atualiza a reserva no banco de dados com seus respectivos dados pela query:
      await db.execute(
        'UPDATE reservas SET id_quarto = ?, id_usuarios = ?, data_entrada = ?, data_saida = ?, valor_total = ? WHERE id = ?',
        [id_quarto, id_usuarios, data_entrada, data_saida, valorTotal, id]
      );
  
      return res.status(200).json({ message: 'Reserva atualizada com sucesso!' });
  
    } catch (error) {
      console.error("Erro ao atualizar a reserva:", error);
      return res.status(500).json({ error: "Erro interno de servidor" });
    }
  }

  // Método para excluir uma reserva
  static async excluirReserva(req, res) {
    const id = req.params.id;
  
    try {
      // Verificar se a reserva com o ID fornecido existe no banco de dados pela query
      // A query seleciona todas as reservas com id específico
      const [reservation] = await db.execute('SELECT * FROM reservas WHERE id = ?', [id]);
      if (reservation.length === 0) {
        return res.status(404).json({ error: 'Reserva não encontrada.' });
      }
  
      // Se a reserva existir, então exclui-a do banco de dados usando o id
      // A query deleta a reserva com id específico
      await db.execute('DELETE FROM reservas WHERE id = ?', [id]);
      return res.status(200).json({ message: 'Reserva excluída com sucesso!' });
    } catch (error) {
      console.error("Erro ao excluir reserva:", error);
      return res.status(500).json({ error: 'Erro ao excluir reserva: ' + error.message });
    }
  }

  // Método para buscar detalhes de todas as reservas

static async detalhesReservas(req, res) {
  const query = 

  /* Essa consulta SQL realiza uma junção (JOIN) entre três tabelas: reservas, quartos e usuarios.
   Ela é usada para buscar detalhes das reservas, 
   incluindo informações sobre o usuário que fez a reserva e o quarto reservado. */
  `
    SELECT 
      r.id AS id_reserva,
      u.nome AS nome_usuario,
      r.data_entrada,
      r.data_saida,
      r.valor_total,
      q.numero_quarto,
      q.valor_diaria,
      DATEDIFF(r.data_saida, r.data_entrada) AS total_dias,
      DATEDIFF(r.data_saida, r.data_entrada) * q.valor_diaria AS total_valor_diarias
    FROM 
      reservas r
    JOIN 
      quartos q ON r.id_quarto = q.id
    JOIN
      usuarios u ON r.id_usuarios = u.id;
  `;

  try {
    const [results] = await db.execute(query);
    return res.status(200).json(results);
  } catch (error) {
    return res.status(500).json({ error: 'Erro ao buscar detalhes das reservas: ' + error.message });
  }
}
}

module.exports = ReservaController;  // Exporta a classe ReservaController para uso em outros módulos
