const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const db = require('../db/connect');

class UsuarioController {
  constructor() {}

  // Método para criar um novo usuário
  static async criarUsuario(req, res) {
    const { nome, email, cpf, senha, telefone, cep } = req.body;

    if (!nome || !email || !cpf || !senha || !telefone || !cep) {
      return res.status(400).json({ error: 'Preencha todos os campos corretamente.' });
    }

    // Validações adicionais de email, cpf, telefone e cep
    if (!isValidEmail(email)) {
      return res.status(400).json({ error: 'Email inválido.' });
    }

    if (!isValidCPF(cpf)) {
      return res.status(400).json({ error: 'CPF inválido.' });
    }

    if (!isValidPhone(telefone)) {
      return res.status(400).json({ error: 'Telefone inválido.' });
    }

    if (!isValidCEP(cep)) {
      return res.status(400).json({ error: 'CEP inválido.' });
    }

    try {
      // Verificar se o CPF ou email já estão cadastrados
      // A query seleciona todos os usuários com email ou cpf específico 
      const [existingUser] = await db.query('SELECT * FROM usuarios WHERE email = ? OR cpf = ?', [email, cpf]);
      if (existingUser.length > 0) {
        return res.status(400).json({ error: 'Usuário já cadastrado com este email ou CPF.' });
      }

      // Criptografar a senha
      const hashedPassword = await bcrypt.hash(senha, 10);

      // Inserir o usuário no banco de dados pela query com seus dados:
      await db.query('INSERT INTO usuarios (nome, email, cpf, senha, telefone, cep) VALUES (?, ?, ?, ?, ?, ?)', 
        [nome, email, cpf, hashedPassword, telefone, cep]);
      
      res.status(201).json({ message: 'Usuário criado com sucesso!' });

    } catch (error) {
      console.error('Erro ao criar usuário:', error.message);
      res.status(500).json({ error: 'Erro ao criar usuário: ' + error.message });
    }
  }

  // Método para buscar todos os usuários
  // A query seleciona todos os usuários
  static async buscarTodosUsuarios(req, res) {
    try {
      const [results] = await db.query('SELECT * FROM usuarios');
      res.status(200).json(results);
    } catch (error) {
      console.error('Erro ao buscar usuários:', error.message);
      res.status(500).json({ error: 'Erro ao buscar usuários: ' + error.message });
    }
  }

  // Método para buscar um usuário por ID
  // A query seleciona todos os usuários que tem um id específico
  static async buscarUsuarioPorId(req, res) {
    const id = req.params.id;

    try {
      const [results] = await db.query('SELECT * FROM usuarios WHERE id = ?', [id]);
      if (results.length === 0) {
        return res.status(404).json({ error: 'Usuário não encontrado.' });
      }
      res.status(200).json(results[0]);
    } catch (error) {
      console.error('Erro ao buscar usuário:', error.message);
      res.status(500).json({ error: 'Erro ao buscar usuário: ' + error.message });
    }
  }

// Método para atualizar um usuário
static async atualizarUsuario(req, res) {
  const id = req.params.id;
  const { nome, email, cpf, senha, telefone, cep } = req.body;

  if (!nome || !email || !cpf || !senha || !telefone || !cep) {
    return res.status(400).json({ error: 'Preencha todos os campos corretamente.' });
  }

  // Validações adicionais
  if (!isValidEmail(email)) {
    return res.status(400).json({ error: 'Email inválido.' });
  }

  if (!isValidCPF(cpf)) {
    return res.status(400).json({ error: 'CPF inválido.' });
  }

  if (!isValidPhone(telefone)) {
    return res.status(400).json({ error: 'Telefone inválido.' });
  }

  if (!isValidCEP(cep)) {
    return res.status(400).json({ error: 'CEP inválido.' });
  }

  try {
    // Verificar se o usuário com o ID fornecido existe no banco de dados
    // A query seleciona todos os usuários com um id específico
    const [user] = await db.query('SELECT * FROM usuarios WHERE id = ?', [id]);
    if (user.length === 0) {
      return res.status(404).json({ error: 'Usuário não encontrado.' });
    }

    // Verificar se o CPF ou email já estão cadastrados por outro usuário
    // A query vai fazer a seleção desses usuários que já estão cadastrados por algum cpf ou email
    const [existingUser] = await db.query('SELECT * FROM usuarios WHERE (email = ? OR cpf = ?) AND id != ?', [email, cpf, id]);
    if (existingUser.length > 0) {
      return res.status(400).json({ error: 'Outro usuário já cadastrado com este email ou CPF.' });
    }

    // Criptografar a senha
    const hashedPassword = await bcrypt.hash(senha, 10);

    // Atualizar o usuário no banco de dados com seus respectivos dados:
    await db.query('UPDATE usuarios SET nome = ?, email = ?, cpf = ?, senha = ?, telefone = ?, cep = ? WHERE id = ?', 
      [nome, email, cpf, hashedPassword, telefone, cep, id]);

    res.status(200).json({ message: 'Usuário atualizado com sucesso!' });

  } catch (error) {
    console.error('Erro ao atualizar usuário:', error.message);
    res.status(500).json({ error: 'Erro ao atualizar usuário: ' + error.message });
  }
}


// Método para excluir um usuário
static async excluirUsuario(req, res) {
  const id = req.params.id;

  try {
    // Verificar se o usuário com o ID fornecido existe no banco de dados
    // A query seleciona todos os usuários com um id específico
    const [user] = await db.query('SELECT * FROM usuarios WHERE id = ?', [id]);
    if (user.length === 0) {
      return res.status(404).json({ error: 'Usuário não encontrado.' });
    }

    // Se o usuário existir, então exclua-o do banco de dados
    // A query deleta o usuário com id específico
    await db.query('DELETE FROM usuarios WHERE id = ?', [id]);
    res.status(200).json({ message: 'Usuário excluído com sucesso!' });
  } catch (error) {
    console.error('Erro ao excluir usuário:', error.message);
    res.status(500).json({ error: 'Erro ao excluir usuário: ' + error.message });
  }
}


  // Método para login
  static async login(req, res) {
    const { email, senha } = req.body;

    if (!email || !senha) {
      return res.status(400).json({ error: 'Preencha todos os campos corretamente.' });
    }

    try {
      // Buscar o usuário pelo email fornecido
      // A query seleciona todos os usuários que tem o email específico ( no caso, apenas um )
      const [users] = await db.query('SELECT * FROM usuarios WHERE email = ?', [email]);
      if (users.length === 0) {
        return res.status(400).json({ error: 'Email ou senha incorretos.' });
      }

      const user = users[0];

      // Comparar a senha fornecida com a senha armazenada
      const isPasswordValid = await bcrypt.compare(senha, user.senha);
      if (!isPasswordValid) {
        return res.status(400).json({ error: 'Email ou senha incorretos.' });
      }

      // Gerar um token JWT (opcional)
      const token = jwt.sign({ id: user.id, email: user.email }, 'secrettoken', { expiresIn: '1h' });

      res.status(200).json({ message: 'Login bem-sucedido!', token });

    } catch (error) {
      console.error('Erro ao fazer login:', error.message);
      res.status(500).json({ error: 'Erro ao fazer login: ' + error.message });
    }
  }
}

// Funções de validação com expressões regulares
function isValidEmail(email) {
  const re = /\S+@\S+\.\S+/;
  return re.test(email);
}

function isValidCPF(cpf) {
  const re = /^\d{11}$/;
  return re.test(cpf);
}

function isValidPhone(phone) {
  const re = /^\d{10,11}$/;
  return re.test(phone);
}

function isValidCEP(cep) {
  const re = /^\d{8}$/;
  return re.test(cep);
}

module.exports = UsuarioController;
