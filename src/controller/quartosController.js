const db = require('../db/connect');

class QuartoController {
  // Método para criar um novo quarto no banco de dados
  static async criarQuarto(req, res) {
    // Extrai os dados do corpo da requisição
    const { andar, numero_quarto, tipo_quarto, valor_diaria, status } = req.body;

    try {
      // Verifica se os campos obrigatórios estão preenchidos
      if (andar === undefined || numero_quarto === undefined || tipo_quarto === "" || valor_diaria === undefined) {
        return res.status(400).json({ error: 'Preencha os campos corretamente.' });
      }

      // Verifica se já existe um quarto com o mesmo número
      // A query seleciona o quarto que tem um número de quarto específico
      const [existingRoom] = await db.execute(
        'SELECT * FROM quartos WHERE numero_quarto = ?',
        [numero_quarto]
      );

      if (existingRoom.length > 0) {
        return res.status(409).json({ error: 'Já existe um quarto com este número.' });
      }

      // Insere os dados do quarto no banco de dados (Andar, número, tipo, valor da diária e o status)
      await db.execute(
        'INSERT INTO quartos (andar, numero_quarto, tipo_quarto, valor_diaria, status) VALUES (?, ?, ?, ?, ?)',
        [andar, numero_quarto, tipo_quarto, valor_diaria, status || 'Disponível']
      );

      // Responde com sucesso
      res.status(201).json({ message: 'Quarto criado com sucesso!' });
    } catch (error) {
      console.error("Erro ao criar quarto:", error);
      return res.status(500).json({ error: 'Erro ao criar quarto: ' + error.message });
    }
  }

  // Método para buscar todos os quartos no banco de dados
  static async buscarTodosQuartos(req, res) {
    try {
      // Busca todos os registros na tabela 'quartos'
      // A query seleciona todos os quartos do banco de dados
      const [results] = await db.execute('SELECT * FROM quartos');
      // Responde com os resultados encontrados
      res.status(200).json(results);
    } catch (error) {
      console.error("Erro ao buscar quartos:", error);
      return res.status(500).json({ error: 'Erro ao buscar quartos: ' + error.message });
    }
  }

  // Método para buscar um quarto por ID no banco de dados
  static async buscarQuartoPorId(req, res) {
    const id = req.params.id;

    try {
      // Busca um registro na tabela 'quartos' com base no ID fornecido
      // A query seleciona o quarto com id específico
      const [results] = await db.execute('SELECT * FROM quartos WHERE id = ?', [id]);
      // Verifica se o quarto foi encontrado
      if (results.length === 0) {
        return res.status(404).json({ error: 'Quarto não encontrado.' });
      }
      // Responde com o quarto encontrado
      res.status(200).json(results[0]);
    } catch (error) {
      console.error("Erro ao buscar quarto:", error);
      return res.status(500).json({ error: 'Erro ao buscar quarto: ' + error.message });
    }
  }

  // Método para atualizar um quarto no banco de dados
  static async atualizarQuarto(req, res) {
    const id = req.params.id;
    const { andar, numero_quarto, tipo_quarto, valor_diaria, status } = req.body;
  
    try {
      // Verificar se o quarto com o ID fornecido existe no banco de dados
      // A query seleciona todos os quartos que tenham um id específico 
      const [room] = await db.execute('SELECT * FROM quartos WHERE id = ?', [id]);
      if (room.length === 0) {
        return res.status(404).json({ error: 'Quarto não encontrado.' });
      }
  
      // Atualiza os dados do quarto com base no ID fornecido
      // A query atualiza os quartos inserindo os seus respectivos dados
      await db.execute(
        'UPDATE quartos SET andar = ?, numero_quarto = ?, tipo_quarto = ?, valor_diaria = ?, status = ? WHERE id = ?',
        [andar, numero_quarto, tipo_quarto, valor_diaria, status, id]
      );
      // Responde com sucesso
      res.status(200).json({ message: 'Quarto atualizado com sucesso!' });
    } catch (error) {
      console.error("Erro ao atualizar quarto:", error);
      return res.status(500).json({ error: 'Erro ao atualizar quarto: ' + error.message });
    }
  }

  // Método para excluir um quarto do banco de dados
  // Método para excluir um quarto do banco de dados
static async excluirQuarto(req, res) {
  const id = req.params.id;

  try {
    // Verificar se o quarto com o ID fornecido existe no banco de dados
    //  A query seleciona todos os quartos com id específico
    const [room] = await db.execute('SELECT * FROM quartos WHERE id = ?', [id]);
    if (room.length === 0) {
      return res.status(404).json({ error: 'Quarto não encontrado.' });
    }

    // Se o quarto existir, então exclui-o do banco de dados
    // A query deleta qualquer quarto pelo id específico passado
    await db.execute('DELETE FROM quartos WHERE id = ?', [id]);
    res.status(200).json({ message: 'Quarto excluído com sucesso!' });
  } catch (error) {
    console.error("Erro ao excluir quarto:", error);
    return res.status(500).json({ error: 'Erro ao excluir quarto: ' + error.message });
  }
}

}

module.exports = QuartoController;
